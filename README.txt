------------
INTRODUCTION
------------

Thanks for checking out SlaughterNeko's PNGTuber Tool! 

SlaughterNeko's PNGTuber Tool is a tool for creating simple, animated and voice-reactive VTuber avatar out of static .png images. This includes optional blinking frames while idle (with customizable parameters) and 
alternative idle poses for variation. All it requires are a few .png images of your VTuber and a bit of time to setup.

This tool is a rough work in progress and features will be added and changed over time.


--------------------
CREDITS AND LICENSES
--------------------

This tool is offered for free and published under the anti-capitalist software license (https://anticapitalist.software/)​. 

If you use this tool my only ask is that you add a credit to me (Twitter: @Devon_Wiersma) on your Twitch/social media page with a link to this tool so others can find it, and send me links to the cool things you make with it :)​



-----
SETUP
-----

Unzip the downloaded folder into a file locations where you have edit permissions (I recommend your "Downloads", "My Documents", or "My Pictures" folders).

To begin you need to create .png files of your VTuber with the following in mind:

- You will need an image of your character in an idle state and a talking state to use the tool at its minimum features. 
- If you are using the optional blinking and alternate idle position you will need images for these as well. Otherwise you should leave these fields blank.
- The images you use should be .png files with background alpha transparency. ***No other filetypes are compatible.***
- The .png of your character should go right up to the edge of the frame of your image. If you leave excess empty space around the edge of your character they may be too large for the window.
- Your images should have a resolution between 300px and 700px, with a square aspect ratio (width = height). Other aspect ratios and sizes may work, but have not been tested. .PNGs which have resolutions which are too large 
will not fit in the window.
- All images you use should be of matching aspect ratio and image resolution to one another, or else your character will change sizes when talking.

When you have your images, copy your .png files into "SlaughterNekosPNGTuberTool/SlaughterNekosPNGTuberTool_Data/StreamingAssets" folder.

Launch the program with your desired microphone set as your default communication device. Ensure your mic is conected and functional before starting the tool, and that the mic does not
have the "Allow applications to take exclusive control of this device" setting enabled (Windows 10).

To import your images, simply type the name of your image file you saved in the StreamingAssets folder into the corresponding text field on the application and click "Update images". 
If you see your images and you don't see any error messages then: congratulations, you're done!

For details on how to use the tool for Streaming, please see the OBS section of this README.



----------
PARAMETERS
----------


====Mic Parameters====

Current Mic Volume: This shows the normalized level of your microphone. Use this to determine what level your Mic Cutoff will be set to.

Mic Cutoff: This value determines the minimum threshold which your Mic Volume must reach in order for your avatar to appear as "speaking". When the Mice Volume is 
lower than this number, the character will be idle. Tune this as appropriate for your mic setup.

Speaking Cooldown: Determines how long (in seconds) the avatar appears as "Speaking" after your Mic Volume drops below the Mic Threshold. A value too low may result in your character 
switching out of their talking state too immediately after speaking, which may not be desired.



====Visual Effects====

Bounce: Toggles the bouncing behaviour while speaking on and off.

Darken on Idle: Toggles the darkening behaviour on and off. This will emulate Discord's feature which dims the character portrait when you're not talking.




====Blinking Effects====

NOTE: Blinking behaviour will only occur while the character is in the idle state and if the blinking image has been imported correctly. If not, nothing will happen.

Blink Min/Max time: These values will determine the minimum and maximum amount of time the character will wait while idling before blinking again (in seconds). The time will be randomized 
between these two values.

Blink Length: How long the blink state of the character will display for (in seconds). Note that values that are too low the blink may play too fast to be noticed on stream.



====Background Palettes====

Clicking the background palette swatches will change the colour of background. This will help when keying out your background in your streaming software.



====File Import Fields====

NOTE: Do not fill out the optional fields if you aren't using the behaviours. Additionally, when filling out these fields do not type the ".png" at the end of your file - only type the name 
of the file itself. If you'd like to test this functionality there are sample Blinking and Alt Idle images included in the StreamingAssets folder.

Idle Image Filename: The name of your default idle image.

Speaking Image Filename: The name of the image which will display while you are speaking.

(Optional) Blinking Image Filename: The name of the image which will display when the character blinks. Clear this field if you do not intend to use this feature.

(Optional) Alt Idle Image Filename: The name of your alternate idle image. When the character returns to the idle state there will be a 50/50 chance of them returning to this idle state or the default one. Clear 
this field if you do not intend to use this feature.



---------
OBS SETUP
---------

After you have followed the setup steps above, open OBS and follow these steps to display your character in its own source: 
1) Add a new Window or Game Source (Window is recommended) of the tool. 
2) Add a crop filter to the source to trim out the Window UI and focus it on the character. 
3) Add a "Color Key" filter to your window and adjust the settings to best remove the background colour you have chosen. 
4) Position the source of your character on your screen.
5) Leave the application running in the background while you stream.



------------
KNOWN ISSUES
------------

the following issues are known limitations with the tool:


- The program automatically detects the first mic input on your PC, so ensure your desired mic is set to your default communication device when booting the program up. 

- Additionally, it will not detect new mics during runtime, so if your mic becomes disconnected for whatever reason you will need to restart the tool to re-enable functionality.

