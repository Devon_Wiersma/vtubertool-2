using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;

[System.Serializable]

public class MicListener : MonoBehaviour
{
    public UIHandler _UIHandler;

    public float MicLoudness;

    public float threshold;
    public float cooldownDelay;

    public float blinkMin, blinkMax;
    public float blinkLength;

    public GameObject speakingImage;
    public GameObject idleImage;
    public GameObject blinkingImage;
    public GameObject altIdleImage;

    GameObject previousIdleImage;

    public GameObject characterObject;

    //DOTween tween;

    public bool canSwitch = true;
    public bool canBlink = true;
    public bool characterBounce = true;
    public bool idleDarken = false;
    public bool altIdleImageUsed;
    public bool blinkImageUsed;
    bool idlingInitiated;


    Vector2 startPosition;
    Vector2 startScale;

    private string _device;

    //mic initialization
    void InitMic()
    {
        if (_device == null) _device = Microphone.devices[0];
        _clipRecord = Microphone.Start(_device, true, 999, 44100);
    }

    void StopMicrophone()
    {
        Microphone.End(_device);
    }


    AudioClip _clipRecord = null;
    int _sampleWindow = 128;

    //get data from microphone into audioclip
    float LevelMax()
    {
        float levelMax = 0;
        float[] waveData = new float[_sampleWindow];
        int micPosition = Microphone.GetPosition(null) - (_sampleWindow + 1); // null means the first microphone
        if (micPosition < 0) return 0;
        _clipRecord.GetData(waveData, micPosition);
        // Getting a peak on the last 128 samples
        for (int i = 0; i < _sampleWindow; i++)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak)
            {
                levelMax = wavePeak;
            }
        }
        return levelMax;
    }

    void Start()
    {
        startPosition = gameObject.transform.position;
        startScale = gameObject.transform.localScale;
    }

    void Update()
    {
        // levelMax equals to the highest normalized value power 2, a small number because < 1
        // pass the value to a static var so we can access it from anywhere
        MicLoudness = LevelMax();

        MicLoudness = MicLoudness * 50000;
        MicLoudness = Mathf.Clamp(MicLoudness, 0, 100);
        _UIHandler.micVolumeText.text = "Current Volume: " + MicLoudness.ToString();
        if (canSwitch)
        {
            //SPEAKING
            if (MicLoudness > threshold)
            {
                //if blinking while the talk happens, stop the blink
                if (blinkImageUsed)
                {
                    StopCoroutine("WaitForBlink");
                    if (blinkingImage.activeSelf == true)
                    {
                        blinkingImage.SetActive(false);
                    }
                }

                DisplayImage("talking");
                canSwitch = false;
                StartCoroutine("ChangeCooldown");

                if (characterBounce)
                    BounceCharacter();
            }

            //NOT SPEAKING
            else if (MicLoudness < threshold)
            {
                DOTween.Kill(characterObject.transform, true);
                speakingImage.SetActive(false);

                //prevents app from frequently swapping between 
                if (idlingInitiated)
                {
                    idlingInitiated = false;
                    if (altIdleImageUsed == false)
                    {
                        DisplayImage("idle");
                    }
                    else
                    {
                        int i = Random.Range(0, 2);
                        print(i);
                        if (i == 0)
                        {
                            DisplayImage("idle");
                        }
                        else
                        {
                            DisplayImage("altIdle");
                        }
                    }

                }

                if (blinkImageUsed)
                {
                    if (canBlink)
                    {
                        StopCoroutine("WaitForBlink");
                        StartCoroutine("WaitForBlink");
                    }
                }
            }
        }
    }

    IEnumerator WaitForBlink()
    {

        //store previous image to re-display it before blinking
        if (altIdleImageUsed)
        {
            if (idleImage.activeSelf)
            {
                previousIdleImage = idleImage;
            }
            else
            {
                previousIdleImage = altIdleImage;
            }

        }

        canBlink = false;

        //determine time to wait before blinking
        yield return new WaitForSeconds(Random.Range(blinkMin, blinkMax));
        DisplayImage("blink");

        //when the blink finishes, return to regular state
        yield return new WaitForSeconds(blinkLength);
        blinkingImage.SetActive(false);
        previousIdleImage.SetActive(true);
        canBlink = true;
        previousIdleImage = idleImage;
    }

    //delay the return to regular state after talking
    IEnumerator ChangeCooldown()
    {
        yield return new WaitForSeconds(cooldownDelay);
        characterObject.transform.position = startPosition;
        characterObject.transform.localScale = startScale;
        canSwitch = true;
        if (blinkImageUsed)
            canBlink = true;
        idlingInitiated = true;
    }

    //EFFECTS
    void BounceCharacter()
    {
        characterObject.transform.DOShakeScale(1, new Vector3(0, 0.0001f, 0), 5, 0, true);
    }



    bool _isInitialized;
    // start mic when scene starts
    void OnEnable()
    {
        InitMic();
        _isInitialized = true;
    }

    //stop mic when loading a new level or quit application
    void OnDisable()
    {
        StopMicrophone();
    }

    void OnDestroy()
    {
        StopMicrophone();
    }

    //displays image corresponding to the string
    void DisplayImage(string imageType)
    {
        blinkingImage.SetActive(false);
        idleImage.SetActive(false);
        altIdleImage.SetActive(false);
        speakingImage.SetActive(false);

        if (idleDarken)
        {
            idleImage.GetComponent<SpriteRenderer>().color = new Color(0.6f, 0.6f, 0.6f, 1);
            altIdleImage.GetComponent<SpriteRenderer>().color = new Color(0.6f, 0.6f, 0.6f, 1);
            print("Images darkened");
        }
        else
        {
            idleImage.GetComponent<SpriteRenderer>().color = Color.white;
            altIdleImage.GetComponent<SpriteRenderer>().color = Color.white;
            print("Images restored");
        }

        switch (imageType)
        {

            case "idle":
                idleImage.SetActive(true);
                break;
            case "talking":
                speakingImage.SetActive(true);
                break;
            case "altIdle":
                altIdleImage.SetActive(true);
                break;
            case "blink":
                blinkingImage.SetActive(true);
                break;
        }
    }



    //// make sure the mic gets started & stopped when application gets focused
    //void OnApplicationFocus(bool focus)
    //{
    //    if (focus)
    //    {
    //        //Debug.Log("Focus");

    //        if (!_isInitialized)
    //        {
    //            //Debug.Log("Init Mic");
    //            InitMic();
    //            _isInitialized = true;
    //        }
    //    }
    //    if (!focus)
    //    {
    //        //Debug.Log("Pause");
    //        StopMicrophone();
    //        //Debug.Log("Stop Mic");
    //        _isInitialized = false;

    //    }
    //}
}