using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{

    public MicListener micListener;

    Texture2D texture;

    public Text thresholdText;
    public Text thresholdPlaceholder;
    public Text delayCooldownText;
    public Text delayCooldownPlaceholder;
    public InputField talkingImageUserText;
    public InputField idleImageUserText;
    public InputField altIdleImageUserText;
    public InputField blinkingImageUserText;

    public Text blinkingMinUserText;
    public Text blinkingMaxUserText;
    public Text blinkingDurationUserText;

    public Text micVolumeText;

    public string talkingImageFileLocation;
    public string idleImageFileLocation;
    public string altIdleImageFileLocation;
    public string blinkingImageFileLocation;

    public Text consoleText;

    int talkingImageWidth;
    int talkingImageHeight;

    int idleImageWidth;
    int idleImageHeight;

    int altIdleImageWidth;
    int altIdleImageHeight;

    int blinkingImageWidth;
    int blinkingImageHeight;

    bool firstLoad = true;

    // Start is called before the first frame update
    void Start()
    {

        InitializeSavedFields();


    }

    void InitializeSavedFields()
    {

        if (!PlayerPrefs.HasKey("IdleImage") && !PlayerPrefs.HasKey("TalkingImage") && !PlayerPrefs.HasKey("BlinkImage") && !PlayerPrefs.HasKey("AltIdleImage"))
        {
            print("first init)");
            idleImageFileLocation = Application.streamingAssetsPath + "/SampleImageIdle1";
            talkingImageFileLocation = Application.streamingAssetsPath + "/SampleImageSpeaking1";
            blinkingImageFileLocation = Application.streamingAssetsPath + "";
            altIdleImageFileLocation = Application.streamingAssetsPath + "";

            idleImageUserText.text = "/SampleImageIdle1";
            talkingImageUserText.text = "/SampleImageSpeaking1";
            blinkingImageUserText.text = "";
            altIdleImageUserText.text = "";
        }
        else
        {
            if (PlayerPrefs.HasKey("IdleImage"))
            {
                idleImageFileLocation = Application.streamingAssetsPath + "/" + PlayerPrefs.GetString("IdleImage") + ".png";
                idleImageUserText.text = PlayerPrefs.GetString("IdleImage");
            }
            else
            {

            }

            if (PlayerPrefs.HasKey("TalkingImage"))
            {
                talkingImageFileLocation = Application.streamingAssetsPath + "/" + PlayerPrefs.GetString("TalkingImage") + ".png";
                talkingImageUserText.text = PlayerPrefs.GetString("TalkingImage");
                print(talkingImageFileLocation);
            }
            else
            {

            }

            if (PlayerPrefs.HasKey("BlinkImage") && PlayerPrefs.GetString("BlinkImage") != "")
            {
                blinkingImageFileLocation = Application.streamingAssetsPath + "/" + PlayerPrefs.GetString("BlinkImage") + ".png";
                blinkingImageUserText.text = PlayerPrefs.GetString("BlinkImage");
            }
            else
            {

            }

            if (PlayerPrefs.HasKey("AltIdleImage") && PlayerPrefs.GetString("AltIdleImage") != "")
            {

                altIdleImageFileLocation = Application.streamingAssetsPath + "/" + PlayerPrefs.GetString("AltIdleImage") + ".png";
                altIdleImageUserText.text = PlayerPrefs.GetString("AltIdleImage");
            }
            else
            {


            }


        }

        UpdateSpeakingImage();
        UpdateIdleImage();
        UpdateBlinkImage();
        UpdateAltIdleImage();


        if (PlayerPrefs.HasKey("MicThreshold"))
        {
            thresholdText.text = PlayerPrefs.GetString("MicThreshold");
            micListener.threshold = float.Parse(thresholdText.text);
        }
        if (PlayerPrefs.HasKey("MicCooldown"))
        {
            delayCooldownText.text = PlayerPrefs.GetString("MicCooldown");
            micListener.cooldownDelay = float.Parse(delayCooldownText.text);
        }
        if (PlayerPrefs.HasKey("BlinkMin"))
        {
            blinkingMinUserText.text = PlayerPrefs.GetString("BlinkMin");
            micListener.blinkMin = float.Parse(blinkingMinUserText.text);
        }
        if (PlayerPrefs.HasKey("BlinkMin"))
        {
            blinkingMaxUserText.text = PlayerPrefs.GetString("BlinkMax");
            micListener.blinkMax = float.Parse(blinkingMaxUserText.text);
        }
        if (PlayerPrefs.HasKey("BlinkLength"))
        {
            blinkingDurationUserText.text = PlayerPrefs.GetString("BlinkLength");
            micListener.blinkLength = float.Parse(blinkingDurationUserText.text);
        }


    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            PlayerPrefs.DeleteAll();
            print("PlayerPrefs Deleted");
        }
    }


    public void LoadImages(string filename)
    {
        byte[] bytes = File.ReadAllBytes(filename);

        texture = new Texture2D(500, 500);
        texture.LoadImage(bytes);

        if (firstLoad)
        {
            if (filename.Contains("SampleImageSpeaking1.png"))
            {
                micListener.speakingImage.GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                talkingImageHeight = texture.height;
                talkingImageWidth = texture.width;
            }
            if (filename.Contains("SampleImageIdle1.png"))
            {
                micListener.idleImage.GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                idleImageHeight = texture.height;
                idleImageWidth = texture.width;
            }
            if (filename.Contains("SampleImageIdleAlt1.png"))
            {
                micListener.altIdleImage.GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                altIdleImageHeight = texture.height;
                altIdleImageWidth = texture.width;
            }
            if (filename.Contains("SampleImageBlinking1.png"))
            {
                micListener.blinkingImage.GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                blinkingImageHeight = texture.height;
                blinkingImageWidth = texture.width;
            }
        }
        else
        {
            if (filename.Contains(talkingImageFileLocation))
            {
                micListener.speakingImage.GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                talkingImageHeight = texture.height;
                talkingImageWidth = texture.width;

            }
            if (filename.Contains(idleImageFileLocation))
            {
                micListener.idleImage.GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                idleImageHeight = texture.height;
                idleImageWidth = texture.width;
            }
            if (filename.Contains(altIdleImageFileLocation))
            {
                micListener.altIdleImage.GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                altIdleImageHeight = texture.height;
                altIdleImageWidth = texture.width;
            }
            if (filename.Contains(blinkingImageFileLocation))
            {
                micListener.blinkingImage.GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                blinkingImageHeight = texture.height;
                blinkingImageWidth = texture.width;
            }
        }


        firstLoad = false;
    }


    public void UpdateThreshold()
    {
        micListener.threshold = float.Parse(thresholdText.text);
        PlayerPrefs.SetString("MicThreshold", micListener.threshold.ToString());
    }

    public void UpdateDelayCooldown()
    {
        micListener.cooldownDelay = float.Parse(delayCooldownText.text);
        PlayerPrefs.SetString("MicCooldown", micListener.cooldownDelay.ToString());
    }

    public void UpdateBlinkMin()
    {
        micListener.blinkMin = float.Parse(blinkingMinUserText.text);
        PlayerPrefs.SetString("BlinkMin", micListener.blinkMin.ToString());
    }
    public void UpdateBlinkMax()
    {
        micListener.blinkMax = float.Parse(blinkingMaxUserText.text);
        PlayerPrefs.SetString("BlinkMax", micListener.blinkMax.ToString());

    }
    public void UpdateBlinkDuration()
    {
        micListener.blinkLength = float.Parse(blinkingDurationUserText.text);
        PlayerPrefs.SetString("BlinkLength", micListener.blinkLength.ToString());
    }

    public void UpdateSpeakingImage()
    {
        if (talkingImageUserText.text != "")
        {
            talkingImageFileLocation = Application.streamingAssetsPath + "/" + talkingImageUserText.text + ".png";
            if (File.Exists(talkingImageFileLocation))
            {
                LoadImages(talkingImageFileLocation);
                print(talkingImageFileLocation);
                PlayerPrefs.SetString("TalkingImage", talkingImageUserText.text);
            }
            else
            {
                consoleText.text = "ERROR: Could not find image with name '" + talkingImageUserText.text + ".png' in the StreamingAssets folder";
            }
        }
        else
        {
            consoleText.text = "ERROR: THE SPEAKING IMAGE DID NOT IMPORT CORRECTLY. Make sure it is a .png image and you typed the file name correctly";
        }
    }
    public void UpdateIdleImage()
    {
        if (idleImageUserText.text != "")
        {
            idleImageFileLocation = Application.streamingAssetsPath + "/" + idleImageUserText.text + ".png";
            if (File.Exists(idleImageFileLocation))
            {
                LoadImages(idleImageFileLocation);
                print(idleImageFileLocation);
                PlayerPrefs.SetString("IdleImage", idleImageUserText.text);
            }
            else
            {
                consoleText.text = "ERROR: Could not find image with name '" + idleImageUserText.text + ".png' in the StreamingAssets folder";
            }
        }
        else
        {
            consoleText.text = "ERROR: THE IDLE IMAGE DID NOT IMPORT CORRECTLY. Make sure it is a .png image and you typed the file name correctly";
        }
    }
    public void UpdateAltIdleImage()
    {
        if (altIdleImageUserText.text != "")
        {

            altIdleImageFileLocation = Application.streamingAssetsPath + "/" + altIdleImageUserText.text + ".png";
            if (File.Exists(altIdleImageFileLocation))
            {
                micListener.altIdleImageUsed = true;
                LoadImages(altIdleImageFileLocation);
                print(altIdleImageFileLocation);
                PlayerPrefs.SetString("AltIdleImage", altIdleImageUserText.text);
            }
            else
            {
                micListener.altIdleImageUsed = false;
                consoleText.text = "ERROR: Could not find image with name '" + altIdleImageUserText.text + ".png' in the StreamingAssets folder";
            }
        }
        else
        {
            micListener.altIdleImageUsed = false;
            PlayerPrefs.DeleteKey("AltIdleImage");
        }

    }
    public void UpdateBlinkImage()
    {
        if (blinkingImageUserText.text != "")
        {
            blinkingImageFileLocation = Application.streamingAssetsPath + "/" + blinkingImageUserText.text + ".png";
            if (File.Exists(blinkingImageFileLocation))
            {
                micListener.blinkImageUsed = true;
                LoadImages(blinkingImageFileLocation);
                print(blinkingImageFileLocation);
                PlayerPrefs.SetString("BlinkImage", blinkingImageUserText.text);
            }
            else
            {
                consoleText.text = "ERROR: Could not find image with name '" + blinkingImageUserText.text + ".png' in the StreamingAssets folder";
            }
        }
        else
        {
            micListener.canBlink = false;
            micListener.blinkImageUsed = false;
            micListener.StopCoroutine("WaitForBlink");
            PlayerPrefs.DeleteKey("BlinkImage");
        }
    }

    public void ChangeBackgroundToGreen()
    {
        Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam.backgroundColor = Color.green;
    }
    public void ChangeBackgroundToBlue()
    {
        Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam.backgroundColor = Color.blue;
    }
    public void ChangeBackgroundToPink()
    {
        Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam.backgroundColor = new Color(255, 0, 255);
    }
    public void ChangeBackgroundToYellow()
    {
        Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam.backgroundColor = Color.yellow;
    }


    public void ToggleBounce()
    {
        micListener.characterBounce = !micListener.characterBounce;
    }

    public void ToggleDarkenIdle()
    {
        micListener.idleDarken = !micListener.idleDarken;
    }
}
